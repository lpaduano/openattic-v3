Trademarks
==========

"Apache HTTP Server", "Apache", and the Apache feather logo are trademarks of
The Apache Software Foundation.

"DRBD |reg|", the DRBD logo, "LINBIT |reg|", and the LINBIT logo are
trademarks or registered trademarks of LINBIT in Austria, the United States
and other countries.

"Linux" is the registered trademark of Linus Torvalds in the U.S. and other
countries.

"Red Hat Linux" and "CentOS" are trademarks of Red Hat, Inc. in the U.S. and
other countries.

"VMware" is a trademark or registered trademark of VMware, Inc. in the United
States and/or other jurisdictions.

All other names and trademarks used herein are the property of their
respective owners.
