.. _post-installation configuration:

Post-installation Configuration
===============================

|oA| Base Configuration
-----------------------

After all the required packages have been installed and a storage pool has
been created, you need to perform the actual |oA| configuration, by running
``oaconfig``::

  # oaconfig install

``oaconfig install`` will start and enable a number of services, initialize
the |oA| database and scan the system for pools and volumes to include.

Changing the Default User Password
----------------------------------

By default, ``oaconfig`` creates a local adminstrative user account
``openattic``, with the same password.

As a security precaution, we strongly recommend to change this password
immediately::

  # oaconfig changepassword openattic
  Changing password for user 'openattic'
  Password: <enter password>
  Password (again): <re-enter password>
  Password changed successfully for user 'openattic'

Now, your |oA| storage system can be managed via the user interface.

See :ref:`getting started` for instructions on how to access the web user
interface.

If you don't want to manage your users locally, consult the chapter
:ref:`admin_auth_methods` for alternative methods for authentication and
authorization.

Installing additional |oA| Modules
----------------------------------

After installing |oA|, you can install additional modules
(``openattic-module-<module-name>``), by using your operating system's native
package manager, i.e.::

  # apt-get install openattic-module-drbd # Debian/Ubuntu
  # yum install openattic-module-btrfs # RHEL/CentOS

.. note::
  Don't forget to run ``oaconfig install`` after installing new modules.

.. _enabling_ceph_support:

Enabling Ceph Support in |oA|
=============================

.. note::
  Ceph support in |oA| is currently developed against Ceph 10.2 aka "Jewel".
  Older Ceph versions may not work as expected. If your Linux distribution
  ships an older version of Ceph (as most currently do), please either use the
  `upstream Ceph package repositories
  <http://docs.ceph.com/docs/master/install/get-packages/>`_ or find an
  alternative package repository for your distribution that provides a version
  of Ceph that meets the requirements. Note that this applies to both the
  version of the Ceph tools installed on the |oA| node as well as the version
  running on your Ceph cluster.

To set up |oA| with Ceph you first have to copy the Ceph administrator keyring
and configuration from your Ceph admin node to your local |oA| system.

From your Ceph admin node, you can perform this step by using ``ceph-deploy``
(assuming that you can perform SSH logins from the admin node into the
|oA| host)::

  # ceph-deploy admin openattic.yourdomain.com

On the |oA| node, you should then have the following files::

  /etc/ceph/ceph.client.admin.keyring
  /etc/ceph/ceph.conf

.. note::
  Please ensure that these files are actually readable by the |oA| user
  (``openattic``) and the Nagios/Icinga user account (usually ``nagios`` or
  ``icinga``) that runs the related Nagios checks. In a default installation,
  these users are added to the group ``openattic``, so it should be sufficient
  to make sure these files are either world-readable or owned and readable by
  this group::

    # chgrp openattic /etc/ceph/ceph.conf /etc/ceph/ceph.client.admin.keyring
    # chmod g+r /etc/ceph/ceph.conf /etc/ceph/ceph.client.admin.keyring

Alternatively, you can copy these files manually.

.. note::
  |oA| supports managing multiple Ceph clusters, provided they have different
  names and FSIDs. You can add another cluster by copying the cluster's admin
  keyring and configuration into ``/etc/ceph`` using a different cluster name,
  e.g. ``development`` instead of the default name ``ceph``::

    /etc/ceph/development.client.admin.keyring
    /etc/ceph/development.conf

The next step is to install the |oA| Ceph module ``openattic-module-ceph`` on your
system::

  # apt-get install openattic-module-ceph
  - or -
  # yum install openattic-module-ceph

The packages should automatically install any additionally required packages.
The last step is to recreate your |oA| configuration::

  # oaconfig install
