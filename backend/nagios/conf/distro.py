"""
 *  Copyright (C) 2011-2016, it-novum GmbH <community@openattic.org>
 *
 *  openATTIC is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

import logging
import os

if os.environ.get("DJANGO_SETTINGS_MODULE"):
    from django.conf import settings


def distro_settings(distro_specific=['/etc/default/openattic', '/etc/sysconfig/openattic']):
    """
    Read the custom settings for a distribution to override defaults. Debian and Ubuntu use
    /etc/default/openattic. SUSE and RedHat use /etc/sysconfig/openattic.

    Returns a dict for non-Django environments
    Sets settings object for Django environments
    """
    logger = logging.getLogger(__name__)

    _settings = {}
    for filename in distro_specific:
        if os.path.isfile(filename):
            logger.debug("Reading %s", filename)
            with open(filename, "r") as f:
                for line in f:
                    line = line.rstrip()
                    if line and not line.startswith('#'):
                        key, value = line.split('=')
                        value = value.strip('"\'')
                        logger.debug("Setting %s=%s", key, value)
                        _settings[key] = value
                        if os.environ.get("DJANGO_SETTINGS_MODULE"):
                            setattr(settings, key, value)

    return _settings
