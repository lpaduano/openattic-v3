��          �       L      L     M     `     h     t     y     �     �     �  
   �     �  <   �     �  	     4        @     E     c     j  y  r     �          	               $     <     J     X     `  T   n     �  	   �  M   �     !     *     I  	   Q   %s Entries matched Command Command Log Date Delete Delete old entries End time Exit Status Loading... Log Entries Log Entries newer than the date you enter here will be kept. Page Search... Select a log entry to see the command's output here. User Waiting for date selection... of {0} unknown Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-24 15:46+0100
PO-Revision-Date: 2014-02-24 17:08
Last-Translator:   <>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n != 1)
X-Translated-Using: django-rosetta 0.6.6
 %s Einträge gefunden Befehl Befehls-Log Datum Löschen Alte Einträge löschen End-Zeitpunkt Rückgabewert Lade... Log-Einträge Log-Einträge die neuer sind als das Datum das Sie hier eingeben werden beibehalten. Seite Suchen... Bitte wählen Sie einen Log-Eintrag aus, um die Ausgabe des Befehls zu sehen. Benutzer Warte auf Wahl eines Datums... von {0} Unbekannt 